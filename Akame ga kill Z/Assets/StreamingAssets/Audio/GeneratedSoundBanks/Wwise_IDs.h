/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AMBIENCE_IN = 211830577U;
        static const AkUniqueID ATACKE = 621228930U;
        static const AkUniqueID ATTACKCHOM = 2155982948U;
        static const AkUniqueID BEE = 446279779U;
        static const AkUniqueID BOXBROKEN = 1553870775U;
        static const AkUniqueID DAMAGE = 1786804762U;
        static const AkUniqueID FOOTSTEPS = 2385628198U;
        static const AkUniqueID FRONTFOOTSTEPCHOM = 1552659653U;
        static const AkUniqueID HITATACK = 2021655222U;
        static const AkUniqueID JUMP = 3833651337U;
        static const AkUniqueID PLAY_AMBIENCE_BIRDSONG = 2647677543U;
        static const AkUniqueID PLAY_OPEN_CHEST = 2675621856U;
        static const AkUniqueID SPACESHIP = 554650843U;
        static const AkUniqueID STATEIN = 908006901U;
        static const AkUniqueID STICK = 1803197027U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace TEST_STATE_GROUP
        {
            static const AkUniqueID GROUP = 1574273375U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID STATE_IN = 690334802U;
                static const AkUniqueID STATE_OUT = 55158445U;
            } // namespace STATE
        } // namespace TEST_STATE_GROUP

    } // namespace STATES

    namespace SWITCHES
    {
        namespace SWITCH_HIT_ELLEN
        {
            static const AkUniqueID GROUP = 264443266U;

            namespace SWITCH
            {
                static const AkUniqueID HIT_CHOMPER = 2849742667U;
            } // namespace SWITCH
        } // namespace SWITCH_HIT_ELLEN

        namespace SWITCH_SURFACE_TYPE
        {
            static const AkUniqueID GROUP = 2818941756U;

            namespace SWITCH
            {
                static const AkUniqueID SWITCH_FOOTSTGRASS = 3458767637U;
                static const AkUniqueID SWITCH_FOOTSTTILE = 3672344733U;
                static const AkUniqueID SWITCH_FOOTSTWATERGRASS = 1210611968U;
            } // namespace SWITCH
        } // namespace SWITCH_SURFACE_TYPE

    } // namespace SWITCHES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MASTER = 4056684167U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID ACTION = 209137191U;
        static const AkUniqueID AMB_INSIDE = 1564366702U;
        static const AkUniqueID AMB_OUTSIDE = 1013211851U;
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID CHARACTER = 436743010U;
        static const AkUniqueID ENEMY = 2299321487U;
        static const AkUniqueID ITEM = 1222624712U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID SFX = 393239870U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
