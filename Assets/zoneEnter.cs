﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zoneEnter : MonoBehaviour
{
    public AK.Wwise.Event zoneevent;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            zoneevent.Post(gameObject);
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            zoneevent.Stop(gameObject);
        }
    }
}
