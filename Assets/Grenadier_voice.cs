using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenadier_voice : MonoBehaviour
{
    public AK.Wwise.Event ActivateShield_; //
    public AK.Wwise.Event pre_hit_; //
    public AK.Wwise.Event StartAttack_; //
    
    public void ActivateShield() //
    {
        ActivateShield_.Post(gameObject);
    }

    public void pre_hit() //
    {
        pre_hit_.Post(gameObject);
    }

    public void StartAttack() //

    {
        StartAttack_.Post(gameObject);
    }
} 
