﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class open_chest : MonoBehaviour
{
    public AK.Wwise.Event openChest;
    public AK.Wwise.Event soundOpenDoor; 
    public AK.Wwise.Event soundOpenDoor02;
    public AK.Wwise.Switch music_switch;
    public void openBox()
    {
        openChest.Post(gameObject);
    }

    public void openDoor()
    {
        soundOpenDoor.Post(gameObject);
    }
    public void OpenDoor02()
    {
        soundOpenDoor02.Post(gameObject);
    }
    public void change_switch()
    {
        music_switch.SetValue(gameObject);
    }
}
