using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu_button : MonoBehaviour
{
    public AK.Wwise.Event start;
    public AK.Wwise.Event select;
    public AK.Wwise.Event end;
    public AK.Wwise.Event exite;
    public AK.Wwise.State myState;
    public AK.Wwise.Event exite_buttone;
    public AK.Wwise.State myState_exite;

    public void start_button()
    {
        start.Post(gameObject);
    }
    public void select_button()
    {
        select.Post(gameObject);
    }
    public void end_button()
    {
        end.Post(gameObject);
    }
    public void exite_button()
    {
        myState.SetValue();
        exite.Post(gameObject);
    }
    public void exite_b()
    {
        myState_exite.SetValue();
        exite_buttone.Post(gameObject);
    }
}
