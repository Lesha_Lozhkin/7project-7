/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID ACTIVATESHIELD = 2443337181U;
        static const AkUniqueID AMBIENCE_IN = 211830577U;
        static const AkUniqueID ATACKE = 621228930U;
        static const AkUniqueID ATTACKCHOM = 2155982948U;
        static const AkUniqueID BEE = 446279779U;
        static const AkUniqueID BIRDS_01 = 1161063803U;
        static const AkUniqueID BIRDS_02 = 1161063800U;
        static const AkUniqueID BOUNCEPLAYER = 1779289586U;
        static const AkUniqueID BOXBROKEN = 1553870775U;
        static const AkUniqueID CAVE_AMB_01 = 1167490011U;
        static const AkUniqueID CAVE_WATER_02 = 3285149595U;
        static const AkUniqueID CELECT = 769058437U;
        static const AkUniqueID CLOTHES_ = 1820017346U;
        static const AkUniqueID CRYSTAL_AMB = 1329896876U;
        static const AkUniqueID CRYSTAL_START = 566660928U;
        static const AkUniqueID CRYSTAL_STOP = 854751980U;
        static const AkUniqueID CRYSTAL_UP = 335435467U;
        static const AkUniqueID DAMAGE = 1786804762U;
        static const AkUniqueID DAMAGEPLAYER = 4151113677U;
        static const AkUniqueID DEATH_GRENADIER_ = 1568651142U;
        static const AkUniqueID DEATH_PLAYER = 3787035475U;
        static const AkUniqueID EMOTEATTACK = 1065449999U;
        static const AkUniqueID EMOTELANDING = 1399379220U;
        static const AkUniqueID END = 529726532U;
        static const AkUniqueID ENDATTACK = 1971183960U;
        static const AkUniqueID EXITE = 3377148410U;
        static const AkUniqueID EXPLOSIONPLAYER = 944803093U;
        static const AkUniqueID FOOSTEPS_CHOMPER = 4007213899U;
        static const AkUniqueID FOOTSTEPPLAYER = 3280701392U;
        static const AkUniqueID FOOTSTEPS = 2385628198U;
        static const AkUniqueID FRONTFOOTSTEPCHOM = 1552659653U;
        static const AkUniqueID HITATACK = 2021655222U;
        static const AkUniqueID HITSOUND_ = 927218416U;
        static const AkUniqueID ITCH_ = 2704638750U;
        static const AkUniqueID JUMP = 3833651337U;
        static const AkUniqueID KVA = 931403583U;
        static const AkUniqueID LAND_PLAYER = 1756568658U;
        static const AkUniqueID MUSIC_BOS = 2781521987U;
        static const AkUniqueID MUSIC_BOSS_STOP = 819129657U;
        static const AkUniqueID MUSIC_LEVEL = 2177735725U;
        static const AkUniqueID MUSIC_ON = 2795543126U;
        static const AkUniqueID MUSIC_STOP = 3227181061U;
        static const AkUniqueID MUSICMENU = 4082046343U;
        static const AkUniqueID MUSICMENU_STOP = 2290215810U;
        static const AkUniqueID OPEN_DOOR_02 = 1151532121U;
        static const AkUniqueID OPENDOOR = 2122995345U;
        static const AkUniqueID PAUSE_ON = 3537680115U;
        static const AkUniqueID PLAY_AMBIENCE_BIRDSONG = 2647677543U;
        static const AkUniqueID PLAY_AMBIENCE_BIRDSONG_01 = 3835523499U;
        static const AkUniqueID PLAY_OPEN_CHEST = 2675621856U;
        static const AkUniqueID PORTAL_01 = 2852231979U;
        static const AkUniqueID PORTAL_02 = 2852231976U;
        static const AkUniqueID PREATACK = 609321358U;
        static const AkUniqueID RAIM_ = 2549077003U;
        static const AkUniqueID SPACESHIP = 554650843U;
        static const AkUniqueID STARSHIPLAND = 3958488008U;
        static const AkUniqueID START = 1281810935U;
        static const AkUniqueID START_GAME = 1114964412U;
        static const AkUniqueID STARTATTACK = 2471346847U;
        static const AkUniqueID STATE_START = 1886795869U;
        static const AkUniqueID STATEIN = 908006901U;
        static const AkUniqueID STICK = 1803197027U;
        static const AkUniqueID STOP_MUSIC = 2837384057U;
        static const AkUniqueID STOPAMBIENCE = 3264771457U;
        static const AkUniqueID SWINGSOUND_ = 1488055665U;
        static const AkUniqueID THROWPLAYER = 1549318500U;
        static const AkUniqueID VOICE_01 = 3101519197U;
        static const AkUniqueID VOICE_02 = 3101519198U;
        static const AkUniqueID VOICE_03 = 3101519199U;
        static const AkUniqueID VOICE_04 = 3101519192U;
        static const AkUniqueID VOICE_05 = 3101519193U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace AMB_STATE
        {
            static const AkUniqueID GROUP = 1622844561U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID OFF = 930712164U;
                static const AkUniqueID ON = 1651971902U;
            } // namespace STATE
        } // namespace AMB_STATE

        namespace EXITE_TO_TWO
        {
            static const AkUniqueID GROUP = 3829243689U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID ON = 1651971902U;
            } // namespace STATE
        } // namespace EXITE_TO_TWO

        namespace MUSIC_STATE_GROUP
        {
            static const AkUniqueID GROUP = 2434766612U;

            namespace STATE
            {
                static const AkUniqueID BOSS = 1560169506U;
                static const AkUniqueID LEVEL01 = 3152323980U;
                static const AkUniqueID LEVEL02 = 3152323983U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace MUSIC_STATE_GROUP

        namespace PAUSE
        {
            static const AkUniqueID GROUP = 3092587493U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID START_P = 2161110606U;
                static const AkUniqueID STOP_P = 1452767832U;
            } // namespace STATE
        } // namespace PAUSE

        namespace STOP
        {
            static const AkUniqueID GROUP = 788884573U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID STOP_MUSIC_MENU = 106912753U;
            } // namespace STATE
        } // namespace STOP

        namespace TEST_STATE_GROUP
        {
            static const AkUniqueID GROUP = 1574273375U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID STATE_IN = 690334802U;
                static const AkUniqueID STATE_OUT = 55158445U;
            } // namespace STATE
        } // namespace TEST_STATE_GROUP

    } // namespace STATES

    namespace SWITCHES
    {
        namespace RAINSWITCH
        {
            static const AkUniqueID GROUP = 985161603U;

            namespace SWITCH
            {
                static const AkUniqueID OFF_RAIN = 1305120933U;
                static const AkUniqueID ON_RAIN = 3409930207U;
            } // namespace SWITCH
        } // namespace RAINSWITCH

        namespace SWITCH_HIT_ELLEN
        {
            static const AkUniqueID GROUP = 264443266U;

            namespace SWITCH
            {
                static const AkUniqueID HIT_CHOMPER = 2849742667U;
            } // namespace SWITCH
        } // namespace SWITCH_HIT_ELLEN

        namespace SWITCH_MUSIC
        {
            static const AkUniqueID GROUP = 2963455281U;

            namespace SWITCH
            {
                static const AkUniqueID BOSS = 1560169506U;
                static const AkUniqueID LEVEL01 = 3152323980U;
                static const AkUniqueID LEVEL02 = 3152323983U;
            } // namespace SWITCH
        } // namespace SWITCH_MUSIC

        namespace SWITCH_SURFACE_TYPE
        {
            static const AkUniqueID GROUP = 2818941756U;

            namespace SWITCH
            {
                static const AkUniqueID SWITCH_FOOTSTGRASS = 3458767637U;
                static const AkUniqueID SWITCH_FOOTSTMETAL = 3619951554U;
                static const AkUniqueID SWITCH_FOOTSTTILE = 3672344733U;
                static const AkUniqueID SWITCH_FOOTSTWATERGRASS = 1210611968U;
            } // namespace SWITCH
        } // namespace SWITCH_SURFACE_TYPE

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID CRYSTAL = 3444057113U;
        static const AkUniqueID DISTANSE = 972228760U;
        static const AkUniqueID EXT_CAMERA_HEIGHT = 1862344870U;
        static const AkUniqueID NEW_GAME_PARAMETER = 3671138082U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID NEW_SOUNDBANK = 4072029455U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID _01_VO = 730095130U;
        static const AkUniqueID _02_UI = 1904168256U;
        static const AkUniqueID _03_HDR = 1491308833U;
        static const AkUniqueID _04_MUSIC = 1653519741U;
        static const AkUniqueID _010_DAMAGE_PC = 3039750224U;
        static const AkUniqueID _020_PC_DEATH = 1459382584U;
        static const AkUniqueID _030_NPC_DEATH = 4068925241U;
        static const AkUniqueID _040_PC_ATACK = 15357134U;
        static const AkUniqueID _050_NPC_ATACK = 3766199419U;
        static const AkUniqueID _060_PC_LOCOMOTION = 328732997U;
        static const AkUniqueID _070_NPC_LOCOMOTION = 1730310198U;
        static const AkUniqueID _080_SOUNDSITEM = 2495767731U;
        static const AkUniqueID _090_FOLY = 2083451743U;
        static const AkUniqueID _100_AMBIENCE = 1065446489U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID REVERB = 348963605U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID FX_REVERB = 469656618U;
        static const AkUniqueID FX_REVERB_01 = 2141424616U;
        static const AkUniqueID FX_REVERB_02 = 2141424619U;
        static const AkUniqueID FX_REVERB_03 = 2141424618U;
        static const AkUniqueID FX_REVERB_04 = 2141424621U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
