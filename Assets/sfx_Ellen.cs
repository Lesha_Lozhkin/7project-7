﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sfx_Ellen : MonoBehaviour

{
    public AK.Wwise.Event damageEllen;

    public AK.Wwise.Event boxDamage;
    public AK.Wwise.Event stop_crystal;
    
    
    public void damage()
    {
        damageEllen.Post(gameObject);
    }

    public void boxBroken()
    {
        boxDamage.Post(gameObject);
    }
    public void stop_crystal_()
    {
        stop_crystal.Post(gameObject);
    }
}
