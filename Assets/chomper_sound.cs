using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chomper_sound : MonoBehaviour
{
    public AK.Wwise.Event kva_kva;
    public AK.Wwise.Event atack;
    public AK.Wwise.Event itch_;
    public AK.Wwise.Event PlayStep_ ;
    public AK.Wwise.Event bite_comper_;
    public AK.Wwise.Event PreAtack_;

    public void kva()
    {
        kva_kva.Post(gameObject);
    }

    public void atack_bite()
    {
        atack.Post(gameObject);
    }

    public void itch ()
    {
        itch_.Post(gameObject);
    }
    public void PlayStep()
    {
        PlayStep_.Post(gameObject);
    }
    public void bite_comper ()
    {
        bite_comper_.Post(gameObject);
    }
    public void PreAtack ()
    {
        PreAtack_.Post(gameObject);
    }
}
