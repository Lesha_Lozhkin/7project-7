﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Gamekit3D; 

public class footsteps : MonoBehaviour
{
    public AK.Wwise.Event footstepsevent;

    //private PlayerInput tpInpute;
    public Animator PlayerAnimator;
    public GameObject FootLeft;
    public GameObject FootRight;
    public bool RightIsPlaying;
    public bool LeftIsPlaying;
    //private string raycast;
    public LayerMask LayerMask;

    public AK.Wwise.Event clothes;

    // Start is called before the first frame update
    void Start()
    {
        //tpInpute = gameObject.GetComponent<PlayerInput>();
    }

    // Update is called once per frame
    void Update()                                                //это если привязывать шаги к кривым CURVE анимации.  "LeftF" "RightF" - эти названия должны совпадать с названиями кривых
    {
        if (Physics.Raycast(FootLeft.transform.position, Vector3.down, out RaycastHit hit, 1f, LayerMask))
        {
            if (PlayerAnimator.GetFloat("LeftF") > 0.01 && !LeftIsPlaying)
            {
                Debug.Log(hit.collider.tag);
                AkSoundEngine.SetSwitch("SWITCH_surface_type", hit.collider.tag, gameObject);
                //SurfaceCheck();
                //AkSoundEngine.SetSwitch("SWITCH_surface_type", raycast, gameObject);
                footstepsevent.Post(gameObject);
                LeftIsPlaying = true;
                clothes.Post(gameObject);
            }

            else if (PlayerAnimator.GetFloat("LeftF") == 0) LeftIsPlaying = false;
        }

        
        if (PlayerAnimator.GetFloat("RightF") > 0.01 && !RightIsPlaying)
            {
                Debug.Log(hit.collider.tag);
                AkSoundEngine.SetSwitch("SWITCH_surface_type", hit.collider.tag, gameObject);
                //SurfaceCheck();
                //if (Physics.Raycast(gameObject.transform.position, Vector3.down, out RaycastHit hit, 0.3f, LayerMask))
                    //SurfaceCheck();
                    //AkSoundEngine.SetSwitch("SWITCH_surface_type", raycast, gameObject);
                    footstepsevent.Post(gameObject);
                    clothes.Post(gameObject);
                RightIsPlaying = true;
            }
            else if (PlayerAnimator.GetFloat("RightF") == 0) RightIsPlaying = false;
        }
    }
    
    /*void SurfaceCheck()

    {

        if (Physics.Raycast(gameObject.transform.position, Vector3.down, out RaycastHit hit, 0.3f, LayerMask))
        {
            Debug.Log(hit.collider.tag);
            raycast = hit.collider.tag;
            AkSoundEngine.SetSwitch("SWITCH_surface_type", raycast, gameObject);
        }
        else
        {
            
        }
    }*/

    /*void FOOTSTEP()                     //это если шаги привязывать к евенту в анимации. FOOTSTEP - это название должно совпадать с названием функции в анимации
    {
        footstepsevent.Post(gameObject);
    }*/
   

